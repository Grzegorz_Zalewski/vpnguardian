﻿using System.Collections.Generic;
using System.ServiceModel;

namespace VpnGuardian.Metadata
{
    [ServiceContract]
    public interface IVpnGuardian
    {
        /// <summary>
        /// </summary>
        [OperationContract]
        List<ResponseMessage> Connect();

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<ResponseMessage> Disconnect();

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<ResponseMessage> CreateNetwork();
        
        /// <summary>
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<ResponseMessage> DeleteNetwork();


        [OperationContract]
        List<ResponseMessage> SaveSettings(string connectionName, string ip, string presharedKey, string login,
            string pass);
    }
}