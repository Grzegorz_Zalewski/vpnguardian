﻿using System.Runtime.Serialization;

namespace VpnGuardian.Metadata
{
    [DataContract]
    public enum ConnectionStatus
    {
        Disconnected,
        Connected,
        Busy
    }
}