﻿namespace VpnGuardian.Metadata
{
    public enum Message
    {
        Connected,
        Disconnected,
        NetworkExist,
        NetworkNotExist,
        NetworkCreated,
        Error,
        SettingsSaved,
        NetworkDeleted,
        NotConnected
    }
}