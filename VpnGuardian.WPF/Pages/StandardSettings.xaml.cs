﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace VpnGuardian.WPF.Pages
{
    /// <summary>
    /// Interaction logic for StandardSettings.xaml
    /// </summary>
    public partial class StandardSettings : UserControl
    {
        public StandardSettings()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            // select first control on the form
            Keyboard.Focus(TextConnectionName);
        }
    }
}
