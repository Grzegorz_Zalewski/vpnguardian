﻿using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace VpnGuardian.WPF.Pages
{
    /// <summary>
    /// Interaction logic for Introduction.xaml
    /// </summary>
    public partial class Introduction : UserControl
    {
        public Introduction()
        {
            InitializeComponent();
        }
    }
}
