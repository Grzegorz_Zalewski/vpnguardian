﻿using System.Windows.Controls;

namespace VpnGuardian.WPF.Content.ModernFrame
{
    /// <summary>
    ///     Interaction logic for Sample.xaml
    /// </summary>
    public partial class Sample : UserControl
    {
        public Sample()
        {
            InitializeComponent();
        }
    }
}