﻿using System.Linq;
using System.Windows.Controls;

namespace VpnGuardian.WPF.Content
{
    /// <summary>
    ///     Interaction logic for ContentLoaderImages.xaml
    /// </summary>
    public partial class ContentLoaderImages : UserControl
    {
        public ContentLoaderImages()
        {
            InitializeComponent();

            LoadImageLinks();
        }

        private async void LoadImageLinks()
        {
            var loader = (FlickrImageLoader) Tab.ContentLoader;

            // load image links and assign to tab list
            Tab.Links = await loader.GetInterestingnessListAsync();

            // select first link
            Tab.SelectedSource = Tab.Links.Select(l => l.Source).FirstOrDefault();
        }
    }
}