﻿using System.Windows.Controls;

namespace VpnGuardian.WPF.Content
{
    /// <summary>
    ///     Interaction logic for ControlsStylesProgressBar.xaml
    /// </summary>
    public partial class ControlsStylesProgressBar : UserControl
    {
        public ControlsStylesProgressBar()
        {
            InitializeComponent();
        }
    }
}