﻿using System.Windows.Controls;

namespace VpnGuardian.WPF.Content
{
    /// <summary>
    ///     Interaction logic for ContentLoaderIntro.xaml
    /// </summary>
    public partial class ContentLoaderIntro : UserControl
    {
        public ContentLoaderIntro()
        {
            InitializeComponent();
        }
    }
}