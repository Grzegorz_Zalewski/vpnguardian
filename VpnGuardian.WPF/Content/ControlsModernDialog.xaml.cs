﻿using System.Windows;
using System.Windows.Controls;
using FirstFloor.ModernUI.Windows.Controls;

namespace VpnGuardian.WPF.Content
{
    /// <summary>
    ///     Interaction logic for ControlsModernDialog.xaml
    /// </summary>
    public partial class ControlsModernDialog : UserControl
    {
        public ControlsModernDialog()
        {
            InitializeComponent();
        }

        private void CommonDialog_Click(object sender, RoutedEventArgs e)
        {
            new ModernDialog
            {
                Title = "Common dialog",
                Content = new LoremIpsum()
            }.ShowDialog();
        }

        private void MessageDialog_Click(object sender, RoutedEventArgs e)
        {
            var btn = MessageBoxButton.OK;
            if (true == ok.IsChecked) btn = MessageBoxButton.OK;
            else if (true == okcancel.IsChecked) btn = MessageBoxButton.OKCancel;
            else if (true == yesno.IsChecked) btn = MessageBoxButton.YesNo;
            else if (true == yesnocancel.IsChecked) btn = MessageBoxButton.YesNoCancel;

            MessageBoxResult result =
                ModernDialog.ShowMessage("This is a simple Modern UI styled message dialog. Do you like it?",
                    "Message Dialog", btn);

            runResult.Text = result.ToString();
        }
    }
}