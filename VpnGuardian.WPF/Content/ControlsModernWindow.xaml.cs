﻿using System.Windows;
using System.Windows.Controls;
using FirstFloor.ModernUI.Windows.Controls;

namespace VpnGuardian.WPF.Content
{
    /// <summary>
    ///     Interaction logic for ControlsModernWindow.xaml
    /// </summary>
    public partial class ControlsModernWindow : UserControl
    {
        public ControlsModernWindow()
        {
            InitializeComponent();
        }

        private void EmptyWindow_Click(object sender, RoutedEventArgs e)
        {
            // create an empty modern window with lorem content
            // the EmptyWindow ModernWindow styles is found in the mui assembly at Assets/ModernWindowEx.xaml

            var wnd = new ModernWindow
            {
                Style = (Style) Application.Current.Resources["EmptyWindow"],
                Content = new LoremIpsum
                {
                    Margin = new Thickness(32)
                },
                Width = 480,
                Height = 480
            };

            wnd.Show();
        }
    }
}