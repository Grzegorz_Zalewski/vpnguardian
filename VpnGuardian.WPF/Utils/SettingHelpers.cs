﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Net;

namespace VpnGuardian.WPF.Utils
{
    public static class SettingHelpers
    {
        public static IAppSettings Rw = new ConfigurationFileAppSettings();


        /// <summary>
        ///     SummaryContactsGUID
        /// </summary>
        public static string ConnectionName
        {
            get { return Rw.ReadSetting(@"ConnectionName", @"VpnGuardianConnection"); }
            set { Rw.SaveSetting(@"ConnectionName", value); }
        }

        public static string LogSoruce
        {
            get { return Rw.ReadSetting(@"LogSoruce", @"VpnGuardianService"); }
            set { Rw.SaveSetting(@"LogSoruce", value); }
        }

        public static string Ip
        {
            get { return Rw.ReadSetting(@"Ip", @"84.19.47.78"); }
            set { Rw.SaveSetting(@"Ip", value); }
        }

        public static string PresharedKey
        {
            get { return Rw.ReadSetting(@"PresharedKey", @"techsoup"); }
            set { Rw.SaveSetting(@"PresharedKey", value); }
        }

        public static NetworkCredential Credential
        {
            get
            {
                return new NetworkCredential(Rw.ReadSetting(@"login", @"gzalewski"),
                    Rw.ReadSetting(@"pass", @"farout"));
            }
            set
            {
                Rw.SaveSetting(@"login", value.UserName);
                Rw.SaveSetting(@"pass", value.Password);
            }
        }

        public static string[] RouteToDelete
        {
            get
            {

                return Rw.ReadSetting(@"RouteToDelete", @"10.0.0.0").Split(';');

            }
            set
            {
                string routes = "";
                foreach (string route in value)
                {
                    routes += route + @";";
                }
                Rw.SaveSetting(@"RouteToDelete", routes.Remove(routes.Length - 1));
            }
        }
        public static string[] RouteToAdd
        {
            get
            {

                return Rw.ReadSetting(@"RouteToAdd", @"192.168.60.0;192.168.54.0;192.168.51.0;").Split(';');

            }
            set
            {
                string routes = "";
                foreach (string route in value)
                {
                    routes += route + @";";
                }
                Rw.SaveSetting(@"RouteToAdd", routes.Remove(routes.Length - 1));
            }
        }
        public static string[] RouteToAdd2
        {
            get
            {

                return Rw.ReadSetting(@"RouteToAdd2", @"10.60.0.0;50.57.0.0;").Split(';');

            }
            set
            {
                string routes = "";
                foreach (string route in value)
                {
                    routes += route + @";";
                }
                Rw.SaveSetting(@"RouteToAdd2", routes.Remove(routes.Length - 1));
            }
        }
    }

    public interface IAppSettings
    {
        T ReadSetting<T>(string settingName, T @default);
        void SaveSetting<T>(string settingName, T @value);
    }

    public class ConfigurationFileAppSettings : IAppSettings
    {
        public void SaveSetting<T>(string settingName, T @value)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

            if (converter.IsValid(@value))
            {
                ConfigurationManager.AppSettings[settingName] = converter.ConvertToString(@value);
            }
            else
            {
                throw new Exception("nie moge zapisać" + settingName + " w konfingu");
            }
        }

        public T ReadSetting<T>(string settingName, T @default)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

            string value = ConfigurationManager.AppSettings[settingName];

            if (!string.IsNullOrEmpty(value))
            {
                if (converter.IsValid(value))
                {
                    return (T)converter.ConvertFromString(value);
                }
            }
            return @default;
        }
    }
}