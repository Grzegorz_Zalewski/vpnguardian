﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VpnGuardian.Metadata;

namespace VpnGuardian.Service.Test
{
    [TestClass]
    public class VpnGuardianServiceTests
    {
        private static VpnGuardianService _instance=new VpnGuardianService(); 
       

        [TestMethod]
        public void B_CreateNetworkTest()
        {
            List<ResponseMessage> results = _instance.CreateNetwork();
            ValidMessages(results);

            Assert.IsTrue(results.Any(x => x.Sender.Equals(Sender.NetworkCreator)));
            Assert.IsTrue(results.Any(x => x.Content.Equals(Message.NetworkExist)||x.Content.Equals(Message.NetworkCreated)));

        }

        [TestMethod]
        public void A_SaveSettingsTest()
        {
            string connectionName=@"VpnGuardianConnection";
            string ip=@"84.19.47.78";
            string presharedKey=@"techsoup";
            string login =@"gzalewski";
            string pass="farout";

            List<ResponseMessage> results = _instance.SaveSettings(connectionName, ip, presharedKey, login, pass);
            ValidMessages(results);
            Assert.IsTrue(results.Any(x => x.Sender.Equals(Sender.Settings)));
            Assert.IsTrue(results.Any(x => x.Content.Equals(Message.SettingsSaved)));
            
        }

        [TestMethod]
        public void C_ConnectTest()
        {
            List<ResponseMessage> results = _instance.Connect();
            ValidMessages(results);
            Assert.IsTrue(results.Any(x => x.Sender.Equals(Sender.Connection)));
            Assert.IsTrue(results.Any(x => x.Content.Equals(Message.Connected)));
            Assert.IsTrue(results.Any(x => x.ConnectionStatus.Equals(ConnectionStatus.Connected)));
        }

        [TestMethod]
        public void D_DisconnectTest()
        {
            List<ResponseMessage> results = _instance.Disconnect();
            ValidMessages(results);
            Assert.IsTrue(results.Any(x => x.Sender.Equals(Sender.Connection)));
            Assert.IsTrue(results.Any(x => x.Content.Equals(Message.Disconnected)));
            Assert.IsTrue(results.Any(x => x.ConnectionStatus.Equals(ConnectionStatus.Disconnected)));
    
        }
        
        [TestMethod]
        public void E_DeleteNetworkTest()
        {
            List<ResponseMessage> results = _instance.DeleteNetwork();
            ValidMessages(results);
            Assert.IsTrue(results.Any(x => x.Sender.Equals(Sender.NetworkCreator)));
            Assert.IsTrue(results.Any(x => x.Content.Equals(Message.NetworkDeleted)||x.Content.Equals(Message.NetworkNotExist)));
            Assert.IsTrue(results.Any(x => x.ConnectionStatus.Equals(ConnectionStatus.Disconnected)));
    
        }

        private static void ValidMessages(List<ResponseMessage> results)
        {

            Assert.IsFalse(results.Any(x => x.IsFail.Equals(null)));
            Assert.IsFalse(results.Any(x => x.ShouldShow.Equals(null)));
            Assert.IsFalse(results.Any(x => x.ConnectionStatus.Equals(null)));
            Assert.IsFalse(results.Any(x => x.Content.Equals(null)));
            Assert.IsFalse(results.Any(x => x.Sender.Equals(null)));
            Assert.IsFalse(results.Any(x => x.Equals(null)));

        }
    }
}