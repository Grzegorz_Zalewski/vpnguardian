﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Navigation;
using FirstFloor.ModernUI.Presentation;
using VpnGuardian.Metadata;
using VpnGuardian.WPF.Utils;

namespace VpnGuardian.WPF
{
    public class ConnectViewModel
        : NotifyPropertyChanged, IDataErrorInfo
    {

        private ICommand _connectCommand;
        private ICommand _disconnectconnectCommand;
        private ICommand _createCommand;
        private ICommand _deleteCommand;
        public event PropertyChangedEventHandler PropertyChanged;


        private bool _isBusy = false;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("IsBusy"));
            }
        }
        public ICommand Connect
        {
            get
            {
                if (_connectCommand == null)
                {
                    _connectCommand = new RelayCommand(
                        param => this.ConnectCommand(),
                        param => this.CanConnect()
                    );
                }
                return _connectCommand;
            }
        }  
        public ICommand Create
        {
            get
            {
                if (_createCommand == null)
                {
                    _connectCommand = new RelayCommand(
                        param => this.CreateCommand(),
                        param => this.CanCreate()
                    );
                }
                return _createCommand;
            }
        }

        private void CreateCommand()
        {
            this._isBusy = true;
            List<ResponseMessage> result = Comunicator.CreateNetwork();
            foreach (ResponseMessage message in result)
            {
                Error = string.Format("{0}{1}{2}{3}{4}", message.Sender, message.Content,
                    message.ConnectionStatus, message.IsFail,
                    message.ShouldShow);
            }
            this._isBusy = false;
        }

        private bool CanCreate()
        {
            return true;
        }

        public ICommand Delete
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _connectCommand = new RelayCommand(
                        param => this.DeleteCommand(),
                        param => this.CanDelete()
                    );
                }
                return _deleteCommand;
            }
        }

        private void DeleteCommand()
        {
            this._isBusy = true;
            List<ResponseMessage> result = Comunicator.DeleteNetwork();
            foreach (ResponseMessage message in result)
            {
                Error = string.Format("{0}{1}{2}{3}{4}", message.Sender, message.Content,
                    message.ConnectionStatus, message.IsFail,
                    message.ShouldShow);
            }
            this._isBusy = false;
        }

        private bool CanDelete()
        {
            return true;
        }

        public ICommand Disconnect
        {
            get
            {
                if (_disconnectconnectCommand == null)
                {
                    _disconnectconnectCommand = new RelayCommand(
                        param => this.DisconnectCommand(),
                        param => this.CanDisconnect()
                    );
                }
                return _disconnectconnectCommand;
            }
        }


        private bool CanConnect()
        {
            return true;
        }

        private void ConnectCommand()
        {
            this._isBusy = true;
            List<ResponseMessage> result = Comunicator.Connect();
            foreach (ResponseMessage message in result)
            {
                Error = string.Format("{0}{1}{2}{3}{4}", message.Sender, message.Content,
                    message.ConnectionStatus, message.IsFail,
                    message.ShouldShow);
            }
            this._isBusy = false;
        }
        private bool CanDisconnect()
        {
            return true;
        }

        private void DisconnectCommand()
        {
            this._isBusy = true;
            List<ResponseMessage> result = Comunicator.Disconnect();
            foreach (ResponseMessage message in result)
            {
                Error = string.Format("{0}{1}{2}{3}{4}", message.Sender, message.Content,
                    message.ConnectionStatus, message.IsFail,
                    message.ShouldShow);
            }

            this._isBusy = false;
        }




        public string this[string columnName]
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Error { get; private set; }
    }
}
