﻿using System.Collections.ObjectModel;
using System.Windows.Controls;
using VpnGuardian.WPF.Utils;

namespace VpnGuardian.WPF.Pages
{
    /// <summary>
    /// Interaction logic for RouteSettings.xaml
    /// </summary>
    /// 
    public enum RouteType
    {
        Usun,
        DodajMaska1,
        DodajMaska2,

    };

    public class Route
    {
        public string Ip { get; set; }
        public RouteType Type { get; set; }
    }
    public partial class RouteSettings : UserControl
    {
        public RouteSettings()
        {
            InitializeComponent();

            ObservableCollection<Route> routes = GetData();


            DG1.DataContext = routes;
        }

        private ObservableCollection<Route> GetData()
        {
            var customers = new ObservableCollection<Route>();
            foreach (var variable in SettingHelpers.RouteToDelete)
            {
                customers.Add( new Route(){ Ip = variable, Type = RouteType.Usun});
            }
            foreach (var variable in SettingHelpers.RouteToAdd)
            {
                customers.Add( new Route(){ Ip = variable, Type = RouteType.DodajMaska1});
            }            foreach (var variable in SettingHelpers.RouteToAdd2)
            {
                customers.Add( new Route(){ Ip = variable, Type = RouteType.DodajMaska2});
            }

            return customers;
        }
    }
}
