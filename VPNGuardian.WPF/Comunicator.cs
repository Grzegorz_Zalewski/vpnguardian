﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using VpnGuardian.Metadata;

namespace VpnGuardian.WPF
{
    public static class Comunicator
    {
        private const string Address = "http://localhost:8000/ServiceModelSamples/service";
        private static HttpBindingBase binding = new BasicHttpBinding();
        private static ChannelFactory<IVpnGuardian> factory = new ChannelFactory<IVpnGuardian>(binding, new EndpointAddress(Address));
        private static IVpnGuardian _vpnGuardian = factory.CreateChannel();
      
         public static List<ResponseMessage> CreateNetwork()
        {
            return _vpnGuardian.CreateNetwork();
        }
        public static List<ResponseMessage> Connect()
        {
            return _vpnGuardian.Connect();
        }

        public static List<ResponseMessage> Disconnect()
        {
            return _vpnGuardian.Disconnect();
        } 
        public static List<ResponseMessage> DeleteNetwork()
        {
            return _vpnGuardian.DeleteNetwork();
        } 
        public static List<ResponseMessage> SaveSettings(string connectionName, string ip, string presharedKey, string login,
            string pass)
        {
            return _vpnGuardian.SaveSettings(connectionName, ip, presharedKey, login, pass);
        }
    }
}
