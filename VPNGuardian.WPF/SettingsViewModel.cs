﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Navigation;
using FirstFloor.ModernUI.Presentation;
using VpnGuardian.WPF.Utils;

namespace VpnGuardian.WPF
{
    public class SettingsViewModel
        : NotifyPropertyChanged, IDataErrorInfo
    {
        private string _logSoruce = SettingHelpers.LogSoruce;
        private string _connectionName = SettingHelpers.ConnectionName;
        private string _login = SettingHelpers.Credential.UserName;    
        private string _pass=SettingHelpers.Credential.Password;    
        private string _presharedKey=SettingHelpers.PresharedKey;    
        private string _ip=SettingHelpers.Ip;
        private ICommand  _saveCommand;
        private ICommand _SaveLoginCommand;
        private bool _progress = false;

       
        public string ConnectionName
        {
            get
            {
                return this._connectionName;
            }
            set
            {
                if (this._connectionName == value) return;
                this._connectionName = value;
                OnPropertyChanged("ConnectionName");
            }
        }
        public string LogSoruce
        {
            get
            {
                return this._logSoruce;
            }
            set
            {
                if (this._logSoruce == value) return;
                this._logSoruce = value;
                OnPropertyChanged("LogSoruce");
            }
        } 
        public string Login
        {
            get { return  this._login; }
            set
            {
                if (this._login == value) return;
                this._login = value;
                OnPropertyChanged("Login");
            }
        }
        public string Pass
        {
            get { return  this._pass; }
            set
            {
                if (this._pass == value) return;
                this._pass = value;
                OnPropertyChanged("Pass");
            }
        }
        public string PresharedKey
        {
            get { return this._presharedKey; }
            set
            {
                if (this._presharedKey == value) return;
                this._presharedKey = value;
                OnPropertyChanged("PresharedKey");
            }
        } 
        public string Ip
        {
            get { return this._ip; }
            set
            {
                if (this._ip == value) return;
                this._ip = value;
                OnPropertyChanged("Ip");
            }
        }


        public string Error
        {
            get { return null; }
        }

        public ICommand SaveStandard
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.SaveStandardCommand(),
                        param => this.CanSaveStandard()
                    );
                }
                return _saveCommand;
            }
        }
        public ICommand SaveLogin
        {
            get
            {
                if (_SaveLoginCommand == null)
                {
                    _SaveLoginCommand = new RelayCommand(
                        param => this.SaveLoginCommand(),
                        param => this.CanSaveLogin()
                    );
                }
                return _SaveLoginCommand;
            }
        }

        private bool CanSaveLogin()
        {
            return false;
        }

        private void SaveLoginCommand()
        {
            this.Progress = true;
            var results = Comunicator.SaveSettings(ConnectionName, Ip, PresharedKey, Login, Pass);

            this.Progress = false;
        }

        public bool Progress
        {
            get { return this._progress; }
            private set
            {
                if (this._progress == value) return;
                this._progress = value;
                OnPropertyChanged("Progress");
            }
        }

        private void SaveStandardCommand ()
        {
            this.Progress = true;
            var results=Comunicator.SaveSettings(ConnectionName, Ip, PresharedKey, Login, Pass);
            
            this.Progress = false;
        }

        private bool CanSaveStandard()
        {
            return false;
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {

                    case "Login": return string.IsNullOrEmpty(this._login) ? "Podaj swój Login" : null;
                    case "ConnectionName": return string.IsNullOrEmpty(this._connectionName) ? "Nazwa połączenia też jest ważna" : null;
                    case "Pass": return string.IsNullOrEmpty(this._pass) ? "Możesz podać nie powiem nikomu" : null;
                    case "Ip": return string.IsNullOrEmpty(this._ip) ? "Musze wiedzieć z kim chcesz się łączyć" : null;
                    case "PresharedKey": return string.IsNullOrEmpty(this._presharedKey) ? "Tą wartość znajdziesz gdzieś w dokumentacji" : null;
                    case "LogSoruce": return string.IsNullOrEmpty(this._logSoruce) ? "Zdefinuj nazwę aplikacji w logach systemowych" : null;    
                    default:  return null;
                }

                return null;
            }
        }
    }
}
