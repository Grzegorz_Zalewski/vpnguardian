cd ../../
@echo off

echo Stopping VpnGuardian server
net stop VpnGuardianService

echo Uninstalling VpnGuardian server
"%windir%\Microsoft.NET\Framework\v4.0.30319\installUtil.exe" /u /LogToConsole=false bin\Debug\VpnGuardian.Service.exe

echo VpnGuardian service unistalled.