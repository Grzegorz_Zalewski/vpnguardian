﻿using System;
using System.Diagnostics;
using System.IO;
using VpnGuardian.Metadata;
using VpnGuardian.Service.Utils;

namespace VpnGuardian.Service.Singletons
{
    public class TraceEvent
    {
        private static TraceEvent _mOInstance;

        public static TraceEvent Instance
        {
            get { return _mOInstance ?? (_mOInstance = new TraceEvent()); }
        }

        static TraceEvent()
        {
            if (!EventLog.SourceExists(SettingHelpers.LogSoruce))
            {
                EventLog.CreateEventSource(SettingHelpers.LogSoruce, @"Application");
            }
        }

        private static void PrivateTrace(EventLogEntryType type, int id, string message)
        {
            try
            {
                EventLog.WriteEntry(SettingHelpers.LogSoruce, message, type, id);
            }
            catch (IOException)
            {
            }
        }

        public static void Trace(EventLogEntryType type, int id, string source, string message)
        {
            PrivateTrace(type, id, source + ": " + message);
        }
    }
}