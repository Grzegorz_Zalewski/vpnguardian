cd ../../
@echo off

echo Installing VPNGuardian server service
"%windir%\Microsoft.NET\Framework\v4.0.30319\installUtil.exe" /LogToConsole=true bin\Debug\VPNGuardian.Service.exe

echo Starting VPNGuardian server service
net start VPNGuardianService

echo VPNGuardian server service installed.

