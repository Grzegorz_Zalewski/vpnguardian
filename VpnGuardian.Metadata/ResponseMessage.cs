﻿using System.Runtime.Serialization;

namespace VpnGuardian.Metadata
{
    [KnownType(typeof (ConnectionStatus))]
    [KnownType(typeof (Message))]
    [KnownType(typeof (Sender))]
    [DataContract]
    public class ResponseMessage
    {
        public ResponseMessage(Message content, Sender sender, ConnectionStatus connectionStatus, bool isfail,
            bool shouldShow)
        {
            {
                Content = content;
                Sender = sender;
                ConnectionStatus = connectionStatus;
                IsFail = isfail;
                ShouldShow = shouldShow;
            }
        }

        public Message Content { get; set; }
        public Sender Sender { get; set; }
        public bool IsFail { get; set; }
        public ConnectionStatus ConnectionStatus { get; set; }
        public bool ShouldShow { get; set; }
    }
}