﻿namespace VpnGuardian.Metadata
{
    public enum Sender
    {
        NetworkCreator,
        Connection,

        Settings
    }
}