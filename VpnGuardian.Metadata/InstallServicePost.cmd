cd ../../../VpnGuardian.Service
@echo off

echo Installing VpnGuardian server service
"%windir%\Microsoft.NET\Framework\v4.0.30319\installUtil.exe" /LogToConsole=true bin\Debug\VpnGuardian.Service.exe

echo Starting VpnGuardian server service
net start VpnGuardianService

echo VpnGuardian server service installed.

