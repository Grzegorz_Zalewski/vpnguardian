﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace VpnGuardian.Service
{
    // Provide the ProjectInstaller class which allows 
    // the service to be installed by the Installutil.exe tool
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            var process = new ServiceProcessInstaller {Account = ServiceAccount.LocalSystem};
            var service = new ServiceInstaller {ServiceName = "VpnGuardianService"};
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}