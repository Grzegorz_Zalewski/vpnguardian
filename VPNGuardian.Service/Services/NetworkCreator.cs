﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotRas;
using VpnGuardian.Metadata;
using VpnGuardian.Service.Singletons;
using VpnGuardian.Service.Utils;

namespace VpnGuardian.Service.Services
{
    internal class NetworkCreator
    {
        public static List<ResponseMessage> CreateNetwork()
        {
            try
            {
                Phonebook.Instance.Open();
                if (Phonebook.Instance.Entries.Contains(SettingHelpers.ConnectionName))
                    return MessageConverter.ToMessage(Message.NetworkExist, Sender.NetworkCreator);
                var entry = RasEntry.CreateVpnEntry(SettingHelpers.ConnectionName, SettingHelpers.Ip,
                    RasVpnStrategy.L2tpFirst,
                    RasDevice.GetDevices().First(x => x.DeviceType.Equals(RasDeviceType.Vpn)));
                entry.Options.UsePreSharedKey = true;
                entry.Options.UseLogOnCredentials = true;
                entry.Options.DisableLcpExtensions = false;
                entry.Options.SoftwareCompression = true;
                entry.Options.RequireMSChap2 = true;
                entry.Options.IPv6RemoteDefaultGateway = false;
                if(!Phonebook.Instance.Entries.Contains(entry))
                Phonebook.Instance.Entries.Add(entry);
                entry.UpdateCredentials(RasPreSharedKey.Client, SettingHelpers.PresharedKey);
                var registry = new ModifyRegistry();
                registry.Write("AssumeUDPEncapsulationContextOnSendRule", 2);
                return MessageConverter.ToMessage(Message.NetworkCreated, Sender.NetworkCreator);
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.NetworkCreator);
            }
        }
    
    public static List<ResponseMessage> DeleteNetwork()
        {
            try
            {
                if (Phonebook.Instance.Entries.Remove(SettingHelpers.ConnectionName))
                {
                    return MessageConverter.ToMessage(Message.NetworkDeleted, Sender.NetworkCreator);
                }
                else
                {
                    return MessageConverter.ToMessage(Message.NetworkNotExist, Sender.NetworkCreator);
                }
                
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.NetworkCreator);
            }
        }
    }
}