﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotRas;
using VpnGuardian.Metadata;
using VpnGuardian.Service.Singletons;
using VpnGuardian.Service.Utils;

namespace VpnGuardian.Service.Services
{
    internal class ConnectingService
    {
        public static List<ResponseMessage> Connect()
        {
            try
            {
                var result = new List<ResponseMessage>();
                if (!Phonebook.Instance.Entries.Contains(SettingHelpers.ConnectionName))
                {
                    result.AddRange(NetworkCreator.CreateNetwork());
                }
                var entry = Phonebook.Instance.Entries.First(x => x.Name.Equals(SettingHelpers.ConnectionName));
                Dialer.Instance.EntryName = entry.Name;
                Dialer.Instance.PhoneBookPath = entry.Owner.Path;
                Dialer.Instance.Credentials = SettingHelpers.Credential;
                Handle.Instance = Dialer.Instance.Dial();
                RouteFixer.SetRoute(SettingHelpers.ConnectionName);
                result.AddRange(MessageConverter.ToMessage(Message.Connected, Sender.Connection));
                
                return result;
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.Connection);
            }
        }

        private static void Cleanup()
        {
            if (Dialer.Instance != null)
            {
                Dialer.Instance.Dispose();
            }

            if (Phonebook.Instance != null)
            {
                Phonebook.Instance.Dispose();
            }
            if (Watcher.Instance != null)
            {
                Watcher.Instance.Dispose();
            }
            if (Handle.Instance != null)
            {
                Handle.ClearHandle();
                Handle.Instance.Dispose();
            }
           
        }

        public static List<ResponseMessage> Disconnect()
        {
            try
            {
                if (Dialer.Instance.IsBusy)
                {
                    // The connection attempt has not been completed, cancel the attempt.
                    Dialer.Instance.DialAsyncCancel();
                }
                else
                {
                    // The connection attempt has completed, attempt to find the connection in the active connections.
                    RasConnection connection =
                        RasConnection.GetActiveConnections()
                            .FirstOrDefault(x => x.EntryName.Equals(SettingHelpers.ConnectionName));
                    if (connection != null)
                    {
                        connection.HangUp();
                    }
                    else
                    {
                        return MessageConverter.ToMessage(Message.NotConnected, Sender.Connection);
                    }
                
                }
                Cleanup();
                return MessageConverter.ToMessage(Message.Disconnected, Sender.Connection);
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.Connection);
            }
        }
    }
}