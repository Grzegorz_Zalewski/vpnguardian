﻿using System.Diagnostics;

namespace VpnGuardian.Service.Utils
{
    internal class CommandExecutor
    {
        #region Static Fields

        /// <summary>
        ///     The instance.
        /// </summary>
        public static readonly CommandExecutor Instance = new CommandExecutor();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="CommandExecutor" /> class from being created.
        ///     Prevents a default instance of the <see cref="CommandExecutor" />
        ///     class from being created.
        /// </summary>
        private CommandExecutor()
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The execute.
        /// </summary>
        /// <param name="command">
        ///     The <paramref name="command" /> .
        /// </param>
        /// <returns>
        ///     The System.String.
        /// </returns>
        public string Execute(string command)
        {
            var psi = new ProcessStartInfo("cmd.exe", string.Concat("/C", command))
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true
            };
            Process p = Process.Start(psi);
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            p.Close();
            return output;
        }

        #endregion
    }
}