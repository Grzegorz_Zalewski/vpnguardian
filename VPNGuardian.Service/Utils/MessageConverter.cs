﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using VpnGuardian.Metadata;
using VpnGuardian.Service.Singletons;
using ConnectionStatus = VpnGuardian.Service.Singletons.ConnectionStatus;

namespace VpnGuardian.Service.Utils
{
    internal static class MessageConverter
    {
        private static int _id;

        internal static List<ResponseMessage> ErorToMessages(Exception ex, Sender sender)
        {
            _id++;
            TraceEvent.Trace(EventLogEntryType.Error, _id, ex.Source,
                string.Format("{0} ST: {1} {2}", ex.Message, ex.StackTrace, ConnectionStatus.Instance));
            return new List<ResponseMessage>
            {
                new ResponseMessage(Message.Error, sender, ConnectionStatus.Instance, true, true)
            };
        }

        internal static List<ResponseMessage> ToMessage(Message message, Sender sender)
        {
            return new List<ResponseMessage>
            {
                new ResponseMessage(message, sender, ConnectionStatus.Instance, false, true)
            };
        }
    }
}