﻿using System.Text.RegularExpressions;
using VpnGuardian.Metadata;

namespace VpnGuardian.Service.Utils
{
    /// <summary>
    /// </summary>
    internal class RouteFixer
    {
        public static void SetRoute(string connectionName)
        {
            string id = GetId(connectionName);
            foreach (string route in SettingHelpers.RouteToDelete)
            {
                CommandExecutor.Instance.Execute(string.Concat("route delete ",route));
            }
            foreach (string route in SettingHelpers.RouteToAdd)
            {
                CommandExecutor.Instance.Execute(string.Concat("route add ", route, @"mask 255.255.255.0 10.100.110.1 metric 11 if",id));
            }
            foreach (string route in SettingHelpers.RouteToAdd2)
            {
                CommandExecutor.Instance.Execute(string.Concat("route add ", route, @"mask 255.255.0.0 10.100.110.1 metric 11 if ", id));
            }

        }

        private static string GetId(string connectionName)
        {
            string route = CommandExecutor.Instance.Execute("route print");
            var regex = new Regex(
                string.Concat("(?<id>[0-9x]+)\\s*\\.+\\s*", connectionName), RegexOptions.IgnoreCase);
            Match match = regex.Match(route);
            return match.Groups["id"].Value;
        }
    }
}