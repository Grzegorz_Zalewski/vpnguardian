﻿using System.ServiceModel;
using System.ServiceProcess;

namespace VpnGuardian.Service
{
    public class VpnGuardianWindowsService : ServiceBase
    {
        public ServiceHost ServiceHost;

        public VpnGuardianWindowsService()
        {
            // Name the Windows Service
            ServiceName = "VpnGuardianService";
        }

        public static void Main()
        {
            Run(new VpnGuardianWindowsService());
        }

        // Start the Windows service.
        protected override void OnStart(string[] args)
        {
            if (ServiceHost != null)
            {
                ServiceHost.Close();
            }

            // Create a ServiceHost for the CalculatorService type and 
            // provide the base address.
            ServiceHost = new ServiceHost(typeof (VpnGuardianService));

            // Open the ServiceHostBase to create listeners and start 
            // listening for messages.
            ServiceHost.Open();
        }

        protected override void OnStop()
        {
            if (ServiceHost != null)
            {
                ServiceHost.Close();
                ServiceHost = null;
            }
        }
    }
}