﻿using System;
using System.Collections.Generic;
using System.Net;
using VpnGuardian.Metadata;
using VpnGuardian.Service.Services;
using VpnGuardian.Service.Utils;

namespace VpnGuardian.Service
{
    // Implement the IVpnGuardian service contract in a service class.
    public class VpnGuardianService : IVpnGuardian
    {
        // Implement the IVpnGuardian methods.
        public List<ResponseMessage> CreateNetwork()
        {
            try
            {
                return NetworkCreator.CreateNetwork();
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.NetworkCreator);
            }
        }
        public List<ResponseMessage> DeleteNetwork()
        {
            try
            {
                var results = Disconnect();
                results.AddRange(NetworkCreator.DeleteNetwork());
                return results;
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.NetworkCreator);
            }
        }


        public List<ResponseMessage> SaveSettings(string connectionName, string ip, string presharedKey, string login,
            string pass)
        {
            try
            {
                SettingHelpers.ConnectionName = connectionName;
                SettingHelpers.Ip = ip;
                SettingHelpers.PresharedKey = presharedKey;
                SettingHelpers.Credential = new NetworkCredential(login, pass);
                return MessageConverter.ToMessage(Message.SettingsSaved, Sender.Settings);
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.Settings);
            }
        }


        public List<ResponseMessage> Connect()
        {
            try
            {
                return ConnectingService.Connect();
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.Connection);
            }
        }

        public List<ResponseMessage> Disconnect()
        {
            try
            {
                return ConnectingService.Disconnect();
            }
            catch (Exception ex)
            {
                return MessageConverter.ErorToMessages(ex, Sender.Connection);
            }
        }
    }
}