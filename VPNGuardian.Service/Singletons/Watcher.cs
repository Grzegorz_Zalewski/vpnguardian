﻿using DotRas;

namespace VpnGuardian.Service.Singletons
{
    public static class Watcher
    {
        private static RasConnectionWatcher _mOInstance;

        public static RasConnectionWatcher Instance
        {
            get { return _mOInstance ?? (_mOInstance = new RasConnectionWatcher()); }
        }
    }
}