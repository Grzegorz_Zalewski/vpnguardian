﻿using DotRas;

namespace VpnGuardian.Service.Singletons
{
    public static class Dialer
    {
        private static RasDialer _mOInstance;
        
        public static RasDialer Instance
        {
            get { return _mOInstance ?? (_mOInstance = new RasDialer()); }
        }
       
        
    }
}