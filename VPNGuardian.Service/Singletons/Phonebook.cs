﻿using DotRas;

namespace VpnGuardian.Service.Singletons
{
    public static class Phonebook
    {
        private static RasPhoneBook _mOInstance;

        public static RasPhoneBook Instance
        {
            get
            {
                if (_mOInstance != null) return _mOInstance;
                _mOInstance = new RasPhoneBook();
                _mOInstance.Open();
                return _mOInstance;
            }
        }
    }
}