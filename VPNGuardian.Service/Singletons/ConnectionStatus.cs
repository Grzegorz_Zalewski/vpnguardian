﻿using System;
using System.Linq;
using DotRas;

namespace VpnGuardian.Service.Singletons
{
    public static class ConnectionStatus
    {
        public static Metadata.ConnectionStatus Instance
        {
            get
            {
                if (Dialer.Instance.IsBusy)
                {
                    return Metadata.ConnectionStatus.Busy;
                }
                try
                {
                    if (!RasConnection.GetActiveConnections().Any())
                    {
                        return Metadata.ConnectionStatus.Disconnected;
                    }
                }
                catch (ArgumentNullException)
                {
                }

                return Metadata.ConnectionStatus.Connected;
            }
        }
    }
}