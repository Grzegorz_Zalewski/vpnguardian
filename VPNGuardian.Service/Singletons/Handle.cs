﻿using System;
using System.IO;
using System.Linq;
using DotRas;
using VpnGuardian.Metadata;
using VpnGuardian.Service.Services;
using VpnGuardian.Service.Utils;

namespace VpnGuardian.Service.Singletons
{
    public static class Handle
    {
        private static RasHandle _mOInstance;

        public static RasHandle Instance
        {
            get { return _mOInstance ?? (_mOInstance = new RasHandle()); }
            set
            {
                _mOInstance = value;
                Dialer.Instance.DialCompleted -= DialCompleted;
                Dialer.Instance.DialCompleted += DialCompleted;
                Dialer.Instance.Timeout = 30000;
                Dialer.Instance.Error -= DialerError;
                Dialer.Instance.Error += DialerError;
            }
        }

        private static void DialerError(object sender, ErrorEventArgs e)
        {
            if (Dialer.Instance.IsBusy)
            {
                // The connection attempt has not been completed, cancel the attempt.
                Dialer.Instance.DialAsyncCancel();
            }
            else
            {
                RasConnection connection =
                    RasConnection.GetActiveConnections()
                        .FirstOrDefault(x => x.EntryName.Equals(SettingHelpers.ConnectionName));
                if (connection != null)
                {
                    connection.HangUp();
                }
            }
            Dialer.Instance.Dial();
        }

        private static void DialCompleted(object sender, EventArgs e)
        {
            Watcher.Instance.Handle = _mOInstance;
            Watcher.Instance.Disconnected -= WatcherDisconnected;
            Watcher.Instance.Disconnected += WatcherDisconnected;
        }

        private static void WatcherDisconnected(object sender, RasConnectionEventArgs e)
        {
            Watcher.Instance.Disconnected += WatcherDisconnected;
            ConnectingService.Connect();
        }

        public static void ClearHandle()
        {
            Watcher.Instance.Disconnected -= WatcherDisconnected;
            Dialer.Instance.DialCompleted -= DialCompleted;
        }
    }
}