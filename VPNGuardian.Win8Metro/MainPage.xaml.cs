﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

using System;
using System.Collections.Generic;
using System.ServiceModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;
using VpnGuardian.Metadata;

namespace VpnGuardian.Win8Metro
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        private const string Address = "http://localhost:8000/ServiceModelSamples/service";
        private readonly IVpnGuardian _vpnGuardian;

        public MainPage()
        {
            InitializeComponent();

            var binding = new BasicHttpBinding();
            var factory = new ChannelFactory<IVpnGuardian>(binding, new EndpointAddress(Address));
            _vpnGuardian = factory.CreateChannel();
        }

        /// <summary>
        ///     Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">
        ///     Event data that describes how this page was reached.  The Parameter
        ///     property is typically used to configure the page.
        /// </param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        public List<ResponseMessage> test()
        {
            List<ResponseMessage> result = _vpnGuardian.CreateNetwork();
            result.AddRange(_vpnGuardian.Connect());
            return result;
        }

        private void ConnectionSwicher_Toggled(object sender, RoutedEventArgs e)
        {
            if (ConnectionSwicher.IsOn)
            {
                try
                {
                    // call the VpnGuardian service and show the result in textbox
                    List<ResponseMessage> result = _vpnGuardian.CreateNetwork();
                    result.AddRange(_vpnGuardian.Connect());
                    foreach (ResponseMessage message in result)
                    {
                        Box.Text = string.Format("{0}{1}{2}{3}{4}", message.Sender, message.Content,
                            message.ConnectionStatus, message.IsFail,
                            message.ShouldShow);
                    }
                    Box.Text = string.Join("\n", result);
                }
                catch (Exception exception)
                {
                    //ConnectionSwicher.IsOn = false;
                    Box.Text = exception.Message + '\n' + exception.StackTrace;
                }
            }
            else
            {
                try
                {
                    List<ResponseMessage> result = _vpnGuardian.CreateNetwork();
                    result.AddRange(_vpnGuardian.Disconnect());
                    foreach (ResponseMessage message in result)
                    {
                        Box.Text += string.Format("{0}{1}{2}{3}{4}", message.Sender, message.Content,
                            message.ConnectionStatus, message.IsFail,
                            message.ShouldShow);
                    }
                    Box.Text = string.Join("\n", result);
                }
                catch (Exception exception)
                {
                    //ConnectionSwicher.IsOn = true;
                    Box.Text = exception.Message + '\n' + exception.StackTrace;
                }
            }
        }
    }
}